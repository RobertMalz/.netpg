namespace MyNotes.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class asd1 : DbMigration
    {
        public override void Up()
        {
         //   DropForeignKey("dbo.Notes", "UserId2", "dbo.AspNetUsers");
         //   DropIndex("dbo.Notes", new[] { "UserId2" });
          //  AddColumn("dbo.Notes", "UserId", c => c.String());
        //    DropColumn("dbo.Notes", "UserId2");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Notes", "UserId2", c => c.String(maxLength: 128));
            DropColumn("dbo.Notes", "UserId");
            CreateIndex("dbo.Notes", "UserId2");
            AddForeignKey("dbo.Notes", "UserId2", "dbo.AspNetUsers", "Id");
        }
    }
}
