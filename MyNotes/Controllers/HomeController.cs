﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MyNotes.Models;

namespace MyNotes.Controllers
{
    public class HomeController : Controller
    {

        private readonly ApplicationDbContext _db = new ApplicationDbContext();


        public ActionResult Index()
        {
            //var notes = from m in _db.Notes
            //            where m.Priority == Priority.High && m.Completed != true
            //            select m;

            var user = User.Identity.GetUserId();

            var notes = from m in _db.Notes
                where m.Priority == Priority.High && m.Completed != true && m.UserId==user
                select m;
            //return Content(notes.ToList().ToString());
            return View(notes.ToList());
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}