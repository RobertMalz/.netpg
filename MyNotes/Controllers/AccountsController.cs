﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using MyNotes.Models;

namespace MyNotes.Controllers
{
    public class AccountsController : Controller
    {
        private ApplicationDbContext db = new ApplicationDbContext();




        // GET: Accounts DONE
        public ActionResult Index()
        {
            var user = User.Identity.GetUserId();

            var accounts = from m in db.Accounts
                           where m.UserId == user
                           select m;

            return View(accounts.ToList());

        }



        // GET: Accounts/Details/5 DONE
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Account account = db.Accounts.Find(id);
            if (account == null)
            {
                return HttpNotFound();
            }
            return View(account);
        }

        // GET: Accounts/Create HALF DONE / TODO: Tworzenie numeru Rachunku
        public ActionResult Create()
        {
            Random rnd = new Random();
            int rach = rnd.Next(100000, 999999);


            var account = new Account
            {
                AccActive = false,
                UserId = User.Identity.GetUserId(),
                AccBalance = 100,
                AccOpenDate = DateTime.Now,
                AccCloseDate = DateTime.Now.AddDays(30),
                AccNumber = "10-10017600-" + rach
            };


            if (ModelState.IsValid)
            {
                db.Accounts.Add(account);
                db.SaveChanges();


                return RedirectToAction("Index");
            }


            return Content("Bład walidacji");
            
        }



        #region "Funkcje niedostępne z poziomu Klienta"

        // POST: Accounts/Create TEGO NIE BĘDZIE
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Create(Account account)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Accounts.Add(account);
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }

        //    ViewBag.UserId = new SelectList(db.Users, "Id", "Email", account.UserId);
        //    return View(account);
        //}



        // GET: Accounts/Edit/5 TEGO NIE BĘDZIE
        //public ActionResult Edit(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Account account = db.Accounts.Find(id);
        //    if (account == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    ViewBag.UserId = new SelectList(db.Users, "Id", "Email", account.UserId);
        //    return View(account);
        //}

        // POST: Accounts/Edit/5
        //[HttpPost]
        //[ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "AccId,AccNumber,AccBalance,AccOpenDate,AccCloseDate,AccActive,UserId")] Account account)
        //{
        //    if (ModelState.IsValid)
        //    {
        //        db.Entry(account).State = EntityState.Modified;
        //        db.SaveChanges();
        //        return RedirectToAction("Index");
        //    }
        //    ViewBag.UserId = new SelectList(db.Users, "Id", "Email", account.UserId);
        //    return View(account);
        //}

        //// GET: Accounts/Delete/5
        //public ActionResult Delete(int? id)
        //{
        //    if (id == null)
        //    {
        //        return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
        //    }
        //    Account account = db.Accounts.Find(id);
        //    if (account == null)
        //    {
        //        return HttpNotFound();
        //    }
        //    return View(account);
        //}

        //// POST: Accounts/Delete/5
        //[HttpPost, ActionName("Delete")]
        //[ValidateAntiForgeryToken]
        //public ActionResult DeleteConfirmed(int id)
        //{
        //    Account account = db.Accounts.Find(id);
        //    db.Accounts.Remove(account);
        //    db.SaveChanges();
        //    return RedirectToAction("Index");
        //}

        //protected override void Dispose(bool disposing)
        //{
        //    if (disposing)
        //    {
        //        db.Dispose();
        //    }
        //    base.Dispose(disposing);
        //}
        #endregion
    }
}
