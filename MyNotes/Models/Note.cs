﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyNotes.Models
{


    public class Note
    {
        [ScaffoldColumn(true)]
        public int Id { get; set; }


        [Required(ErrorMessage = "Tytuł musi zostać podany.")]
        [DisplayName("Tytuł Notatki")]
        public string Title { get; set; }

        [DataType(DataType.MultilineText)]
        [DisplayName("Szczegółowe informacje")]
        public string Content { get; set; }

        [Required(ErrorMessage = "Podaj priorytet notatki.")]
        [DisplayName("Priorytet")]
        public Priority Priority { get; set; }

        [DisplayName("Czy nieważna?")]
        public bool Completed { get; set; }

        public string UserId { get; set; }
     

       [ForeignKey("UserId")]
       public ApplicationUser User { get; set; }

        //public string UserId2 { get; set; }

        //[ForeignKey("UserId2")]
        //public ApplicationUser User { get; set; }

    }

    public enum Priority
    {
        [Display(Name = "Ważne")]
        High,
        [Display(Name = "Średnie")]
        Medium,
        [Display(Name = "Mało istotne")]
        Low
    }

}