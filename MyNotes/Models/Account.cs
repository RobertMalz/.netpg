﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace MyNotes.Models
{



    [Table("Rachunki")]
    public class Account
    {
        [Key]
        public int AccId { get; set; }
        [DisplayName("Numer Rachunku")]
        public string AccNumber { get; set; }
        [DisplayName("Saldo")]
        public float AccBalance { get; set; }
        [DisplayName("Data Otwarcia Rachunku")]
        public DateTime AccOpenDate { get; set; }
        [DisplayName("Data Zamknięcia Rachunku")]
        public DateTime AccCloseDate { get; set; }
        public bool AccActive { get; set; }
        public string UserId { get; set; }

        [ForeignKey("UserId")]
        public ApplicationUser User { get; set; }


    }

    //Lokaty oraz ich okresowość
    [Table("Lokaty")]
    public class Deposit : Account
    {
        public string DepPeriod { get; set; }
        public string DepSourceAccNumber { get; set; }

   
    }



}