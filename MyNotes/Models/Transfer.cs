﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MyNotes.Models
{
    public class Transfer
    {
        public int TrnId { get; set; }

        public int AccId { get; set; }

        public string UserId { get; set; }

        public string TrnTitle { get; set; }
        public string TrnAccNo { get; set; }
        public string TrnBnfName { get; set; }
        //TODO: REGEXP na wprowadzany rachunek odbiorcy
        public string TrnBnfAddr { get; set; }
        public float TrnAmount { get; set; }
        public DateTime TrnDate { get; set; }


        public int test { get; set; }

    }
}